package com.utils.dcsreconciliationscheduler.dto;

import java.math.BigDecimal;
import java.util.Date;

/**
 * Created by Hasan BOZACIOGLU on 12.01.2020.
 */
public class ExternalModificationLiableTransactionDto {
    private String hostClientDebtAccountingReferenceValue;
    private String hostClientContactReferenceValue;
    private String hostClientLiableTransactionReferenceValue;
    private String hostDebitCreditType;
    private Date hostTransactionDate;
    private BigDecimal hostTransactionAmount;
    private BigDecimal hostTransactionTax1Amount;
    private BigDecimal hostTransactionTax2Amount;
    private String hostTransactionType;
    private String h_crr_typ;
    private Boolean isCloned;

    public String getHostClientDebtAccountingReferenceValue() {
        return hostClientDebtAccountingReferenceValue;
    }

    public void setHostClientDebtAccountingReferenceValue(String hostClientDebtAccountingReferenceValue) {
        this.hostClientDebtAccountingReferenceValue = hostClientDebtAccountingReferenceValue;
    }

    public String getHostClientContactReferenceValue() {
        return hostClientContactReferenceValue;
    }

    public void setHostClientContactReferenceValue(String hostClientContactReferenceValue) {
        this.hostClientContactReferenceValue = hostClientContactReferenceValue;
    }

    public String getHostClientLiableTransactionReferenceValue() {
        return hostClientLiableTransactionReferenceValue;
    }

    public void setHostClientLiableTransactionReferenceValue(String hostClientLiableTransactionReferenceValue) {
        this.hostClientLiableTransactionReferenceValue = hostClientLiableTransactionReferenceValue;
    }

    public String getHostDebitCreditType() {
        return hostDebitCreditType;
    }

    public void setHostDebitCreditType(String hostDebitCreditType) {
        this.hostDebitCreditType = hostDebitCreditType;
    }

    public Date getHostTransactionDate() {
        return hostTransactionDate;
    }

    public void setHostTransactionDate(Date hostTransactionDate) {
        this.hostTransactionDate = hostTransactionDate;
    }

    public BigDecimal getHostTransactionAmount() {
        return hostTransactionAmount;
    }

    public void setHostTransactionAmount(BigDecimal hostTransactionAmount) {
        this.hostTransactionAmount = hostTransactionAmount;
    }

    public BigDecimal getHostTransactionTax1Amount() {
        return hostTransactionTax1Amount;
    }

    public void setHostTransactionTax1Amount(BigDecimal hostTransactionTax1Amount) {
        this.hostTransactionTax1Amount = hostTransactionTax1Amount;
    }

    public BigDecimal getHostTransactionTax2Amount() {
        return hostTransactionTax2Amount;
    }

    public void setHostTransactionTax2Amount(BigDecimal hostTransactionTax2Amount) {
        this.hostTransactionTax2Amount = hostTransactionTax2Amount;
    }

    public String getHostTransactionType() {
        return hostTransactionType;
    }

    public void setHostTransactionType(String hostTransactionType) {
        this.hostTransactionType = hostTransactionType;
    }

    public String getH_crr_typ() {
        return h_crr_typ;
    }

    public void setH_crr_typ(String h_crr_typ) {
        this.h_crr_typ = h_crr_typ;
    }

    public Boolean getCloned() {
        return isCloned;
    }

    public void setCloned(Boolean cloned) {
        isCloned = cloned;
    }
}
