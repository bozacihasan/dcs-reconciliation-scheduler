package com.utils.dcsreconciliationscheduler.dto;

/**
 * Created by Hasan BOZACIOGLU on 12.01.2020.
 */
public interface ReconciliationFileType {
    String RECONCILIATION = "RECONCILIATION";
    String RE00101_EX_DBT_BAL = "RE00101_EX_DBT_BAL";
    String RE00102_EX_MOD_LIA_TR = "RE00102_EX_MOD_LIA_TR";
    String RE00103_EX_EXPNS_OPLF_BAL = "RE00103_EX_EXPNS_OPLF_BAL";
    String RE00104_EX_CLNT_F_TR = "RE00104_EX_CLNT_F_TR";
    String RE00105_PYMNT_CUR_ACC_TR = "RE00105_PYMNT_CUR_ACC_TR";
}
