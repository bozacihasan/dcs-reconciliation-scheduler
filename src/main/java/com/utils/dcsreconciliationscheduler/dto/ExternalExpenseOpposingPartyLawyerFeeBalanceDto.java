package com.utils.dcsreconciliationscheduler.dto;

import java.math.BigDecimal;
import java.util.Date;

/**
 * Created by Hasan BOZACIOGLU on 12.01.2020.
 */
public class ExternalExpenseOpposingPartyLawyerFeeBalanceDto {

    private String hostClientDebtAccountingReferenceValue;
    private String hostClientContactReferenceValue;
    private String hostClientWarningLetterReferenceValue;
    private String hostClientTransactionReferenceValue;
    private Boolean hostIsCloned;
    private String hostIsApproved;
    private String h_cry_typ;
    private Date hostTransactionDate;
    private BigDecimal hostDiscountPrincipalDebtSumAmount;
    private BigDecimal hostCollectionCreditSumAmount;

    public String getHostClientDebtAccountingReferenceValue() {
        return hostClientDebtAccountingReferenceValue;
    }

    public void setHostClientDebtAccountingReferenceValue(String hostClientDebtAccountingReferenceValue) {
        this.hostClientDebtAccountingReferenceValue = hostClientDebtAccountingReferenceValue;
    }

    public String getHostClientContactReferenceValue() {
        return hostClientContactReferenceValue;
    }

    public void setHostClientContactReferenceValue(String hostClientContactReferenceValue) {
        this.hostClientContactReferenceValue = hostClientContactReferenceValue;
    }

    public String getHostClientWarningLetterReferenceValue() {
        return hostClientWarningLetterReferenceValue;
    }

    public void setHostClientWarningLetterReferenceValue(String hostClientWarningLetterReferenceValue) {
        this.hostClientWarningLetterReferenceValue = hostClientWarningLetterReferenceValue;
    }

    public String getHostClientTransactionReferenceValue() {
        return hostClientTransactionReferenceValue;
    }

    public void setHostClientTransactionReferenceValue(String hostClientTransactionReferenceValue) {
        this.hostClientTransactionReferenceValue = hostClientTransactionReferenceValue;
    }

    public Boolean getHostIsCloned() {
        return hostIsCloned;
    }

    public void setHostIsCloned(Boolean hostIsCloned) {
        this.hostIsCloned = hostIsCloned;
    }

    public String getHostIsApproved() {
        return hostIsApproved;
    }

    public void setHostIsApproved(String hostIsApproved) {
        this.hostIsApproved = hostIsApproved;
    }

    public String getH_cry_typ() {
        return h_cry_typ;
    }

    public void setH_cry_typ(String h_cry_typ) {
        this.h_cry_typ = h_cry_typ;
    }

    public Date getHostTransactionDate() {
        return hostTransactionDate;
    }

    public void setHostTransactionDate(Date hostTransactionDate) {
        this.hostTransactionDate = hostTransactionDate;
    }

    public BigDecimal getHostDiscountPrincipalDebtSumAmount() {
        return hostDiscountPrincipalDebtSumAmount;
    }

    public void setHostDiscountPrincipalDebtSumAmount(BigDecimal hostDiscountPrincipalDebtSumAmount) {
        this.hostDiscountPrincipalDebtSumAmount = hostDiscountPrincipalDebtSumAmount;
    }

    public BigDecimal getHostCollectionCreditSumAmount() {
        return hostCollectionCreditSumAmount;
    }

    public void setHostCollectionCreditSumAmount(BigDecimal hostCollectionCreditSumAmount) {
        this.hostCollectionCreditSumAmount = hostCollectionCreditSumAmount;
    }
}
