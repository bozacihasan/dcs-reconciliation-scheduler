package com.utils.dcsreconciliationscheduler.dto;

import java.math.BigDecimal;
import java.util.Date;

/**
 * Created by Hasan BOZACIOGLU on 12.01.2020.
 */
public class ExternalClientFeeTransactionDto {

    private String hostClientDebtAccountingReferenceValue;
    private String hostClientContactReferenceValue;
    private String hostClientTransactionReferenceValue;
    private Date hostTransactionDate;
    private String h_cry_typ;
    private BigDecimal hostClientFeeTransactionAmount;
    private BigDecimal hostClientFeeTransactionTax1Amount;
    private BigDecimal hostClientFeeTransactionTax2Amount;

    public String getHostClientDebtAccountingReferenceValue() {
        return hostClientDebtAccountingReferenceValue;
    }

    public void setHostClientDebtAccountingReferenceValue(String hostClientDebtAccountingReferenceValue) {
        this.hostClientDebtAccountingReferenceValue = hostClientDebtAccountingReferenceValue;
    }

    public String getHostClientContactReferenceValue() {
        return hostClientContactReferenceValue;
    }

    public void setHostClientContactReferenceValue(String hostClientContactReferenceValue) {
        this.hostClientContactReferenceValue = hostClientContactReferenceValue;
    }

    public String getHostClientTransactionReferenceValue() {
        return hostClientTransactionReferenceValue;
    }

    public void setHostClientTransactionReferenceValue(String hostClientTransactionReferenceValue) {
        this.hostClientTransactionReferenceValue = hostClientTransactionReferenceValue;
    }

    public Date getHostTransactionDate() {
        return hostTransactionDate;
    }

    public void setHostTransactionDate(Date hostTransactionDate) {
        this.hostTransactionDate = hostTransactionDate;
    }

    public String getH_cry_typ() {
        return h_cry_typ;
    }

    public void setH_cry_typ(String h_cry_typ) {
        this.h_cry_typ = h_cry_typ;
    }

    public BigDecimal getHostClientFeeTransactionAmount() {
        return hostClientFeeTransactionAmount;
    }

    public void setHostClientFeeTransactionAmount(BigDecimal hostClientFeeTransactionAmount) {
        this.hostClientFeeTransactionAmount = hostClientFeeTransactionAmount;
    }

    public BigDecimal getHostClientFeeTransactionTax1Amount() {
        return hostClientFeeTransactionTax1Amount;
    }

    public void setHostClientFeeTransactionTax1Amount(BigDecimal hostClientFeeTransactionTax1Amount) {
        this.hostClientFeeTransactionTax1Amount = hostClientFeeTransactionTax1Amount;
    }

    public BigDecimal getHostClientFeeTransactionTax2Amount() {
        return hostClientFeeTransactionTax2Amount;
    }

    public void setHostClientFeeTransactionTax2Amount(BigDecimal hostClientFeeTransactionTax2Amount) {
        this.hostClientFeeTransactionTax2Amount = hostClientFeeTransactionTax2Amount;
    }
}
