package com.utils.dcsreconciliationscheduler.dto;

import java.math.BigDecimal;
import java.util.Date;

/**
 * Created by Hasan BOZACIOGLU on 12.01.2020.
 */
public class ExternalDebtBalanceDto {

    private String hostClientDebtAccountingReferenceValue;
    private String hostClientContactReferenceValue;
    private String h_cry_typ;
    private Date hostCutoffDate;
    private BigDecimal hostBalancePrincipalAmount;
    private BigDecimal hostBalanceFixedIncomeAmount;
    private BigDecimal hostBalanceFixedIncomeTax1Amount;
    private BigDecimal hostBalanceFixedIncomeTax2Amount;
    private BigDecimal hostBalanceFixedCommonAmount;
    private BigDecimal hostBalanceFixedCommonTax1Amount;
    private BigDecimal hostBalanceFixedCommonTax2Amount;
    private BigDecimal hostBalanceRatedIncomeAmount;
    private BigDecimal hostBalanceRatedIncomeTax1Amount;
    private BigDecimal hostBalanceRatedIncomeTax2Amount;
    private BigDecimal hostBalanceExpenseOpposingPartyLawyerFeeAmount;
    private String hostOpeningClosingStatus;
    private String hostOpeningClosingType;
    private String hostBalancingPlanReferenceValue;
    private String hostBalancingPlanType;
    private Boolean hostBalancingPlanStatus;

    public String getHostClientDebtAccountingReferenceValue() {
        return hostClientDebtAccountingReferenceValue;
    }

    public void setHostClientDebtAccountingReferenceValue(String hostClientDebtAccountingReferenceValue) {
        this.hostClientDebtAccountingReferenceValue = hostClientDebtAccountingReferenceValue;
    }

    public String getHostClientContactReferenceValue() {
        return hostClientContactReferenceValue;
    }

    public void setHostClientContactReferenceValue(String hostClientContactReferenceValue) {
        this.hostClientContactReferenceValue = hostClientContactReferenceValue;
    }

    public String getH_cry_typ() {
        return h_cry_typ;
    }

    public void setH_cry_typ(String h_cry_typ) {
        this.h_cry_typ = h_cry_typ;
    }

    public Date getHostCutoffDate() {
        return hostCutoffDate;
    }

    public void setHostCutoffDate(Date hostCutoffDate) {
        this.hostCutoffDate = hostCutoffDate;
    }

    public BigDecimal getHostBalancePrincipalAmount() {
        return hostBalancePrincipalAmount;
    }

    public void setHostBalancePrincipalAmount(BigDecimal hostBalancePrincipalAmount) {
        this.hostBalancePrincipalAmount = hostBalancePrincipalAmount;
    }

    public BigDecimal getHostBalanceFixedIncomeAmount() {
        return hostBalanceFixedIncomeAmount;
    }

    public void setHostBalanceFixedIncomeAmount(BigDecimal hostBalanceFixedIncomeAmount) {
        this.hostBalanceFixedIncomeAmount = hostBalanceFixedIncomeAmount;
    }

    public BigDecimal getHostBalanceFixedIncomeTax1Amount() {
        return hostBalanceFixedIncomeTax1Amount;
    }

    public void setHostBalanceFixedIncomeTax1Amount(BigDecimal hostBalanceFixedIncomeTax1Amount) {
        this.hostBalanceFixedIncomeTax1Amount = hostBalanceFixedIncomeTax1Amount;
    }

    public BigDecimal getHostBalanceFixedIncomeTax2Amount() {
        return hostBalanceFixedIncomeTax2Amount;
    }

    public void setHostBalanceFixedIncomeTax2Amount(BigDecimal hostBalanceFixedIncomeTax2Amount) {
        this.hostBalanceFixedIncomeTax2Amount = hostBalanceFixedIncomeTax2Amount;
    }

    public BigDecimal getHostBalanceFixedCommonAmount() {
        return hostBalanceFixedCommonAmount;
    }

    public void setHostBalanceFixedCommonAmount(BigDecimal hostBalanceFixedCommonAmount) {
        this.hostBalanceFixedCommonAmount = hostBalanceFixedCommonAmount;
    }

    public BigDecimal getHostBalanceFixedCommonTax1Amount() {
        return hostBalanceFixedCommonTax1Amount;
    }

    public void setHostBalanceFixedCommonTax1Amount(BigDecimal hostBalanceFixedCommonTax1Amount) {
        this.hostBalanceFixedCommonTax1Amount = hostBalanceFixedCommonTax1Amount;
    }

    public BigDecimal getHostBalanceFixedCommonTax2Amount() {
        return hostBalanceFixedCommonTax2Amount;
    }

    public void setHostBalanceFixedCommonTax2Amount(BigDecimal hostBalanceFixedCommonTax2Amount) {
        this.hostBalanceFixedCommonTax2Amount = hostBalanceFixedCommonTax2Amount;
    }

    public BigDecimal getHostBalanceRatedIncomeAmount() {
        return hostBalanceRatedIncomeAmount;
    }

    public void setHostBalanceRatedIncomeAmount(BigDecimal hostBalanceRatedIncomeAmount) {
        this.hostBalanceRatedIncomeAmount = hostBalanceRatedIncomeAmount;
    }

    public BigDecimal getHostBalanceRatedIncomeTax1Amount() {
        return hostBalanceRatedIncomeTax1Amount;
    }

    public void setHostBalanceRatedIncomeTax1Amount(BigDecimal hostBalanceRatedIncomeTax1Amount) {
        this.hostBalanceRatedIncomeTax1Amount = hostBalanceRatedIncomeTax1Amount;
    }

    public BigDecimal getHostBalanceRatedIncomeTax2Amount() {
        return hostBalanceRatedIncomeTax2Amount;
    }

    public void setHostBalanceRatedIncomeTax2Amount(BigDecimal hostBalanceRatedIncomeTax2Amount) {
        this.hostBalanceRatedIncomeTax2Amount = hostBalanceRatedIncomeTax2Amount;
    }

    public BigDecimal getHostBalanceExpenseOpposingPartyLawyerFeeAmount() {
        return hostBalanceExpenseOpposingPartyLawyerFeeAmount;
    }

    public void setHostBalanceExpenseOpposingPartyLawyerFeeAmount(BigDecimal hostBalanceExpenseOpposingPartyLawyerFeeAmount) {
        this.hostBalanceExpenseOpposingPartyLawyerFeeAmount = hostBalanceExpenseOpposingPartyLawyerFeeAmount;
    }

    public String getHostOpeningClosingStatus() {
        return hostOpeningClosingStatus;
    }

    public void setHostOpeningClosingStatus(String hostOpeningClosingStatus) {
        this.hostOpeningClosingStatus = hostOpeningClosingStatus;
    }

    public String getHostOpeningClosingType() {
        return hostOpeningClosingType;
    }

    public void setHostOpeningClosingType(String hostOpeningClosingType) {
        this.hostOpeningClosingType = hostOpeningClosingType;
    }

    public String getHostBalancingPlanReferenceValue() {
        return hostBalancingPlanReferenceValue;
    }

    public void setHostBalancingPlanReferenceValue(String hostBalancingPlanReferenceValue) {
        this.hostBalancingPlanReferenceValue = hostBalancingPlanReferenceValue;
    }

    public String getHostBalancingPlanType() {
        return hostBalancingPlanType;
    }

    public void setHostBalancingPlanType(String hostBalancingPlanType) {
        this.hostBalancingPlanType = hostBalancingPlanType;
    }

    public Boolean getHostBalancingPlanStatus() {
        return hostBalancingPlanStatus;
    }

    public void setHostBalancingPlanStatus(Boolean hostBalancingPlanStatus) {
        this.hostBalancingPlanStatus = hostBalancingPlanStatus;
    }
}
