package com.utils.dcsreconciliationscheduler.dto;

import java.util.Date;

/**
 * Created by Hasan BOZACIOGLU on 12.01.2020.
 */
public class ReconciliationFileDto {
    private String reconciliationType;
    private Date reconciliationBeginDate;
    private Date reconciliationEndDate;
    private Boolean debtFilterIsActive;
    private SubReconciliationFileDto subReconciliationFileDto;

    public String getReconciliationType() {
        return reconciliationType;
    }

    public void setReconciliationType(String reconciliationType) {
        this.reconciliationType = reconciliationType;
    }

    public Date getReconciliationBeginDate() {
        return reconciliationBeginDate;
    }

    public void setReconciliationBeginDate(Date reconciliationBeginDate) {
        this.reconciliationBeginDate = reconciliationBeginDate;
    }

    public Date getReconciliationEndDate() {
        return reconciliationEndDate;
    }

    public void setReconciliationEndDate(Date reconciliationEndDate) {
        this.reconciliationEndDate = reconciliationEndDate;
    }

    public Boolean getDebtFilterIsActive() {
        return debtFilterIsActive;
    }

    public void setDebtFilterIsActive(Boolean debtFilterIsActive) {
        this.debtFilterIsActive = debtFilterIsActive;
    }

    public SubReconciliationFileDto getSubReconciliationFileDto() {
        return subReconciliationFileDto;
    }

    public void setSubReconciliationFileDto(SubReconciliationFileDto subReconciliationFileDto) {
        this.subReconciliationFileDto = subReconciliationFileDto;
    }

    @Override
    public String toString() {
        return "ReconciliationFileDto{" +
                "reconciliationFileType='" + reconciliationType + '\'' +
                ", reconciliationBeginDate='" + reconciliationBeginDate + '\'' +
                ", reconciliationEndDate='" + reconciliationEndDate + '\'' +
                ", debtFilterIsActive=" + debtFilterIsActive +
                '}';
    }
}
