package com.utils.dcsreconciliationscheduler.dto;

import java.math.BigDecimal;
import java.util.Date;

/**
 * Created by Hasan BOZACIOGLU on 12.01.2020.
 */
public class PaymentCurrentAccountTransactionDto {

    private String hostTransactionListProcessId;
    private String hostBusinessContact;
    private String h_cry_typ;
    private Date hostTransactionDate;
    private String  hostPaymentCurrentAccountTransactionType;
    private BigDecimal hostPaymentCreditSumAmount;

    public String getHostTransactionListProcessId() {
        return hostTransactionListProcessId;
    }

    public void setHostTransactionListProcessId(String hostTransactionListProcessId) {
        this.hostTransactionListProcessId = hostTransactionListProcessId;
    }

    public String getHostBusinessContact() {
        return hostBusinessContact;
    }

    public void setHostBusinessContact(String hostBusinessContact) {
        this.hostBusinessContact = hostBusinessContact;
    }

    public String getH_cry_typ() {
        return h_cry_typ;
    }

    public void setH_cry_typ(String h_cry_typ) {
        this.h_cry_typ = h_cry_typ;
    }

    public Date getHostTransactionDate() {
        return hostTransactionDate;
    }

    public void setHostTransactionDate(Date hostTransactionDate) {
        this.hostTransactionDate = hostTransactionDate;
    }

    public String getHostPaymentCurrentAccountTransactionType() {
        return hostPaymentCurrentAccountTransactionType;
    }

    public void setHostPaymentCurrentAccountTransactionType(String hostPaymentCurrentAccountTransactionType) {
        this.hostPaymentCurrentAccountTransactionType = hostPaymentCurrentAccountTransactionType;
    }

    public BigDecimal getHostPaymentCreditSumAmount() {
        return hostPaymentCreditSumAmount;
    }

    public void setHostPaymentCreditSumAmount(BigDecimal hostPaymentCreditSumAmount) {
        this.hostPaymentCreditSumAmount = hostPaymentCreditSumAmount;
    }
}
