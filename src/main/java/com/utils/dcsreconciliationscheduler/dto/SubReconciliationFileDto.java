package com.utils.dcsreconciliationscheduler.dto;

import java.util.List;

/**
 * Created by Hasan BOZACIOGLU on 12.01.2020.
 */
public class SubReconciliationFileDto {
    private String fileType;
    private String fileName;
    private String filePath;
    private String fileFullPath;
    private List<String> content;

    public String getFileType() {
        return fileType;
    }

    public void setFileType(String fileType) {
        this.fileType = fileType;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getFilePath() {
        return filePath;
    }

    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }

    public String getFileFullPath() {
        return fileFullPath;
    }

    public void setFileFullPath(String fileFullPath) {
        this.fileFullPath = fileFullPath;
    }

    public List<String> getContent() {
        return content;
    }

    public void setContent(List<String> content) {
        this.content = content;
    }

    @Override
    public String toString() {
        return "FileDto{" +
                "fileType='" + fileType + '\'' +
                ", fileName='" + fileName + '\'' +
                ", filePath='" + filePath + '\'' +
                ", fileFullPath='" + fileFullPath + '\'' +
                '}';
    }
}
