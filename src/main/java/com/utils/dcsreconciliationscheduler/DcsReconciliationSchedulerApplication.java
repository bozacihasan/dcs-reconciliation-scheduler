package com.utils.dcsreconciliationscheduler;

import com.utils.dcsreconciliationscheduler.utils.sftp.SftpService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;

import java.util.Date;

@Slf4j
@SpringBootApplication
public class DcsReconciliationSchedulerApplication implements ApplicationRunner, CommandLineRunner {

    @Autowired
    private ApplicationContext appContext;

    public static void main(String[] args) {
        SpringApplication.run(DcsReconciliationSchedulerApplication.class, args);
    }

    @Override
    public void run(ApplicationArguments args) throws Exception {
        log.info("DcsReconciliationSchedulerApplication running... Start time : {}", new Date());
        SftpService sftpService = appContext.getBean(SftpService.class);
        sftpService.getSftpReconciliationFile();
	}

    @Override
    public void run(String... args) throws Exception {
//		String[] beans = appContext.getBeanDefinitionNames();
//		Arrays.sort(beans);
//		for (String bean : beans) {
//			System.out.println("********"+bean);
//		}
    }
}
