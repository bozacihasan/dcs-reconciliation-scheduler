package com.utils.dcsreconciliationscheduler.utils.sftp;

import com.utils.dcsreconciliationscheduler.dto.SubReconciliationFileDto;
import com.utils.dcsreconciliationscheduler.dto.ReconciliationFileDto;
import com.utils.dcsreconciliationscheduler.dto.ReconciliationFileType;
import com.utils.dcsreconciliationscheduler.service.BindExternalListToInternalDtoService;
import com.utils.dcsreconciliationscheduler.utils.DcsUtils;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.BooleanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.integration.sftp.session.SftpSession;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Hasan BOZACIOGLU on 11.01.2020.
 */

@Slf4j
@Service("sftpService")
public class SftpServiceImpl implements SftpService, ReconciliationFileType {

    @Value("${host.sftp.path}")
    private String path;

    @Value("${host.sftp.path.prefix}")
    private String path_prefix;

    @Value("${host.sftp.file.extension}")
    private String file_extension;

    @Value("${host.sftp.file.name.delimiter}")
    private String file_delimiter;

    @Value("${host.sftp.file.content.delimiter}")
    private String content_delimiter;

    @Autowired
    private SftpSessionFactory sftpSessionFactory;

    @Autowired
    private BindExternalListToInternalDtoService bindExternalListToInternalDtoService;

    private SftpSession sftpSession = null;

    private List<String> includeFileNameList;

    @PostConstruct
    private void init() {

        this.sftpSession = sftpSessionFactory.getSftpSession();

        includeFileNameList = new ArrayList<String>();

        includeFileNameList.add(RE00101_EX_DBT_BAL);
        includeFileNameList.add(RE00102_EX_MOD_LIA_TR);
        includeFileNameList.add(RE00103_EX_EXPNS_OPLF_BAL);
        includeFileNameList.add(RE00104_EX_CLNT_F_TR);
        includeFileNameList.add(RE00105_PYMNT_CUR_ACC_TR);
    }

    @Override
    public List<ReconciliationFileDto> getSftpReconciliationFile() {

        if (sftpSession == null)
            return null;

        if (!DcsUtils.checkSftpSessionIsOpen(sftpSession)) {
            sftpSession = sftpSessionFactory.getSftpSession();
        }

        List<ReconciliationFileDto> reconciliationFileDtoList = new ArrayList<ReconciliationFileDto>();

        try {

            String expectedFileName = DcsUtils.getExpectedFileNameByFileExtension(
                    DcsUtils.DATE_FORMAT_YYYYMMDD,
                    file_delimiter,
                    file_extension,
                    RECONCILIATION);

            String fullFilePath = path_prefix + path + path_prefix + expectedFileName;
            Boolean isFileExist = sftpSession.exists(fullFilePath);

            if (BooleanUtils.isFalse(isFileExist)) {
                log.info("File not exist : {}", fullFilePath);
                return null;
            }

            log.info("File exist : {}", fullFilePath);

            List<String> reconciliationFileDetailList = DcsUtils.readSftpFileRowByRow(sftpSession, fullFilePath);

            log.info(reconciliationFileDetailList.toString());

            reconciliationFileDetailList.forEach(item -> {

                String[] reconciliationDetails = DcsUtils.splitRecordViaDelimiter(item, content_delimiter);

                ReconciliationFileDto reconciliationFileDto = bindExternalListToInternalDtoService
                        .bindToReconciliationFileDto(reconciliationDetails);

                if (reconciliationFileDto != null) {

                    SubReconciliationFileDto subReconciliationFileDto = getSubReconciliationFileDto(
                            reconciliationFileDto.getReconciliationType());

                    if (subReconciliationFileDto != null) {
                        reconciliationFileDto.setSubReconciliationFileDto(subReconciliationFileDto);
                        reconciliationFileDtoList.add(reconciliationFileDto);

                    } else {
                        log.info("SubReconciliationFileDto couldn't found for reconciliationType : {}"
                                , reconciliationFileDto.getReconciliationType());
                    }

                } else {
                    log.info("ReconciliationDetails could't bind for expectedFileName: {}, binding item : {}"
                            , expectedFileName
                            , item);
                }
            });

            if (CollectionUtils.isEmpty(reconciliationFileDtoList)) {
                log.info("reconciliationFileDtoList is null");
                return null;
            }

            log.info("reconciliationFileDtoList : {}", reconciliationFileDtoList);


        } catch (Exception e) {
            log.error("getSftpReconciliationFile() Unknown Exception");
            e.printStackTrace();
        }

        return reconciliationFileDtoList;
    }

    @Override
    public SubReconciliationFileDto getSubReconciliationFileDto(String reconciliationType) {

        if (sftpSession == null)
            return null;

        if (!DcsUtils.checkSftpSessionIsOpen(sftpSession)) {
            sftpSession = sftpSessionFactory.getSftpSession();
        }

        if (!DcsUtils.equalsOneOfThem(reconciliationType, includeFileNameList)) {
            log.info("Undefined reconciliationType, reconciliationType : {}", reconciliationType);
            return null;
        }

        SubReconciliationFileDto fileDto = null;

        try {
            String expectedFileName = DcsUtils.getExpectedFileNameByFileExtension(
                    DcsUtils.DATE_FORMAT_YYYYMMDD,
                    file_delimiter,
                    file_extension,
                    reconciliationType);

            String fullFilePath = path_prefix + path + path_prefix + expectedFileName;
            Boolean isFileExist = sftpSession.exists(fullFilePath);

            if (BooleanUtils.isFalse(isFileExist)) {
                log.info("File not exist : {}", fullFilePath);
                return null;
            }

            log.info("File exist : {}", fullFilePath);

            fileDto = new SubReconciliationFileDto();

            fileDto.setFileType(reconciliationType);
            fileDto.setFileName(expectedFileName);
            fileDto.setFilePath(path_prefix + path);
            fileDto.setFileFullPath(path_prefix + path + path_prefix + expectedFileName);
            fileDto.setContent(DcsUtils.readSftpFileRowByRow(sftpSession, fullFilePath));

            log.info("---> {}", fileDto.toString());

        } catch (Exception e) {
            log.error("getSubReconciliationFileDto() Unknown Exception");
            e.printStackTrace();
        }

        return fileDto;
    }

}
