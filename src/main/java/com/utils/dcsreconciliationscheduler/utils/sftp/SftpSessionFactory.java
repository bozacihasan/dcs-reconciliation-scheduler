package com.utils.dcsreconciliationscheduler.utils.sftp;

import org.springframework.integration.sftp.session.SftpSession;

/**
 * Created by Hasan BOZACIOGLU on 12.01.2020.
 */
public interface SftpSessionFactory {
    SftpSession getSftpSession();
}
