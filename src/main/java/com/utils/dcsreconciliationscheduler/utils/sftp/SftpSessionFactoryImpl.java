package com.utils.dcsreconciliationscheduler.utils.sftp;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.integration.sftp.session.DefaultSftpSessionFactory;
import org.springframework.integration.sftp.session.SftpSession;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

/**
 * Created by Hasan BOZACIOGLU on 12.01.2020.
 */

@Slf4j
@Component("sftpSessionFactory")
public class SftpSessionFactoryImpl implements SftpSessionFactory {

    @Value("${host.sftp.host}")
    private String host;

    @Value("${host.sftp.port}")
    private int port;

    @Value("${host.sftp.username}")
    private String username;

    @Value("${host.sftp.password}")
    private String password;

    private DefaultSftpSessionFactory sftpSession;

    @PostConstruct
    public void init() {
        log.info("initializing sftpService ");
        initSftpSessionFactory();
    }

    private void initSftpSessionFactory() {

        DefaultSftpSessionFactory factory = new DefaultSftpSessionFactory();

        factory.setHost(host);
        factory.setPort(port);
        factory.setAllowUnknownKeys(true);
        factory.setUser(username);
        factory.setPassword(password);
        this.sftpSession = factory;

        log.info("initSftpSessionFactory() for {}", host);
    }

    @Override
    public SftpSession getSftpSession() {

        SftpSession session = null;
        log.info("getSftpSession() for {}", host);

        try {
            session = sftpSession.getSession();
        } catch (IllegalStateException e) {
            log.error("Couldn't create sftpSession for : {}", host);
            e.printStackTrace();
        }

        return session;
    }
}
