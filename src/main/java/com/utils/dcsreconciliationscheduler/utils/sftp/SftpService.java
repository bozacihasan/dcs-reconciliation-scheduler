package com.utils.dcsreconciliationscheduler.utils.sftp;

import com.utils.dcsreconciliationscheduler.dto.ReconciliationFileDto;
import com.utils.dcsreconciliationscheduler.dto.SubReconciliationFileDto;

import java.util.List;

/**
 * Created by Hasan BOZACIOGLU on 11.01.2020.
 */

public interface SftpService {
    List<ReconciliationFileDto> getSftpReconciliationFile();

    SubReconciliationFileDto getSubReconciliationFileDto(String reconciliationType);
}
