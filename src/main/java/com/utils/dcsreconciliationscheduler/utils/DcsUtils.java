package com.utils.dcsreconciliationscheduler.utils;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.time.DateFormatUtils;
import org.springframework.integration.sftp.session.SftpSession;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by Hasan BOZACIOGLU on 11.01.2020.
 */
@Slf4j
public class DcsUtils {

    public final static String DATE_FORMAT_YYYYMMDD = "YYYYMMDD";

    public static List<String> filterFileName(List<String> fileNameList, List<String> includeFileNameList) {

        List<String> filteredFile = fileNameList.stream()
                .filter(c -> equalsOneOfThem(c, includeFileNameList))
                .collect(Collectors.toList());

        return filteredFile;
    }

    public static Boolean equalsOneOfThem(String item, List<String> stringList) {

        if (StringUtils.isEmpty(item) || CollectionUtils.isEmpty(stringList)) {
            log.info("item or stringList is null");
            return Boolean.FALSE;
        }

        for (String list : stringList) {
            if (list.equals(item))
                return Boolean.TRUE;
        }

        return Boolean.FALSE;
    }

    public static List<String> readSftpFileRowByRow(SftpSession session, String fullFilePath) {

        List<String> fileRowList = null;

        if (BooleanUtils.isFalse(checkSftpSessionIsOpen(session))) {
            log.info("SftpSession is not Open!");
            return null;
        }

        if (StringUtils.isEmpty(fullFilePath)) {
            log.info("fullFilePath is null");
            return null;
        }

        try {

            ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
            session.read(fullFilePath, outputStream);
            BufferedReader bufferedReader = new BufferedReader(new StringReader(new String(outputStream.toByteArray())));
            fileRowList = bufferedReader.lines().collect(Collectors.toList());

            return fileRowList;

        } catch (IOException e) {
            log.info("current file not reading, fullFilePath : {}", fullFilePath);
            e.printStackTrace();
        }

        return fileRowList;
    }

    public static String[] splitRecordViaDelimiter(String record, String delimiter) {

        log.info("will be split current record : {}", record);

        if (StringUtils.isEmpty(record)) {
            log.info("Current record is null");
            return null;
        }

        String[] params = record.split(delimiter);

        if (ArrayUtils.isEmpty(params)) {
            log.info("Current record couldn't split.");
            return null;
        }

        return params;
    }

    public static Boolean checkSftpSessionIsOpen(SftpSession session) {
        return (session != null && BooleanUtils.isTrue(session.isOpen()))
                ? Boolean.TRUE
                : Boolean.FALSE;
    }

    public static List<String> getExpectedFileNameListByFileExtension(String dateFormat,
                                                                      String file_delimiter,
                                                                      String fileExtension,
                                                                      List<String> includeFileNameList) {
        DateFormatUtils.format(new Date(), dateFormat);
        List<String> currentFileNameList = new ArrayList<>(includeFileNameList.size());
        includeFileNameList.forEach(p ->
                currentFileNameList.add(DateFormatUtils.format(new Date(), dateFormat) + file_delimiter + p + fileExtension));

        return currentFileNameList;
    }

    public static String getExpectedFileNameByFileExtension(String dateFormat,
                                                            String file_delimiter,
                                                            String fileExtension,
                                                            String fileName) {
        DateFormatUtils.format(new Date(), dateFormat);
        return DateFormatUtils.format(new Date(), dateFormat) + file_delimiter + fileName + fileExtension;
    }
}
