package com.utils.dcsreconciliationscheduler.service;

import com.utils.dcsreconciliationscheduler.dto.*;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.springframework.stereotype.Service;

import java.text.ParseException;
import java.util.Date;

/**
 * Created by Hasan BOZACIOGLU on 12.01.2020.
 */

@Slf4j
@Service("bindExternalListToInternalDtoService")
public class BindExternalListToInternalDtoServiceImpl implements BindExternalListToInternalDtoService {

    @Override
    public ExternalDebtBalanceDto bindToExternalDebtBalanceDto(String[] params) {

        ExternalDebtBalanceDto externalDebtBalanceDto = null;

        if (ArrayUtils.isEmpty(params)) {
            log.info("ExternalDebtBalanceDto params is null");
            return null;
        }

        try {
            externalDebtBalanceDto = new ExternalDebtBalanceDto();


        } catch (Exception e) {
            log.info("params couldn't bind to ExternalDebtBalanceDto, :{}", params.toString());
            return null;
        }

        return externalDebtBalanceDto;
    }

    @Override
    public ExternalClientFeeTransactionDto bindToExternalClientFeeTransactionDto(String[] params) {

        return null;
    }

    @Override
    public ExternalExpenseOpposingPartyLawyerFeeBalanceDto bindToExternalExpenseOpposingPartyLawyerFeeBalanceDto(String[] params) {

        return null;
    }

    @Override
    public ExternalModificationLiableTransactionDto bindToExternalModificationLiableTransactionDto(String[] params) {

        return null;
    }

    @Override
    public PaymentCurrentAccountTransactionDto bindToPaymentCurrentAccountTransactionDto(String[] params) {

        return null;
    }

    @Override
    public ReconciliationFileDto bindToReconciliationFileDto(String[] params) {

        ReconciliationFileDto reconciliationFileDto = null;

        if (ArrayUtils.isEmpty(params)) {
            log.info("ReconciliationFileDto params is null");
            return null;
        }

        try {
            reconciliationFileDto = new ReconciliationFileDto();
            reconciliationFileDto.setReconciliationType(getStringValueByIndex(params, 0));
            reconciliationFileDto.setReconciliationBeginDate(convertToDate(getStringValueByIndex(params, 1)));
            reconciliationFileDto.setReconciliationEndDate(convertToDate(getStringValueByIndex(params, 2)));
            reconciliationFileDto.setDebtFilterIsActive(convertToBoolean(getStringValueByIndex(params, 3)));

        } catch (Exception e) {
            log.info("params couldn't bind to ReconciliationFileDto, :{}", params.toString());
            return null;
        }

        return reconciliationFileDto;
    }

    private String getStringValueByIndex(String[] params, int index) throws IndexOutOfBoundsException {

        String s = null;

        try {
            s = params[index];
        } catch (IndexOutOfBoundsException e) {
            log.error("Index not found params[{}]", index);
            throw new IndexOutOfBoundsException("Index not found");
        }

        if (StringUtils.isBlank(s)) {
            log.error("params[{}] is null", index);
            throw new NullPointerException("null params");
        }

        return s;
    }

    private Date convertToDate(String item) throws ParseException {

        if (StringUtils.isBlank(item)) {
            return null;
        }

        return DateUtils.parseDate(item, "yyyyMMdd");
    }

    private Boolean convertToBoolean(String item) {
        if (StringUtils.isBlank(item)) {
            return null;
        }

        return BooleanUtils.isTrue(Boolean.valueOf(item))
                ? Boolean.TRUE
                : Boolean.FALSE;
    }
}
