package com.utils.dcsreconciliationscheduler.service;

import com.utils.dcsreconciliationscheduler.dto.*;

import java.util.List;

/**
 * Created by Hasan BOZACIOGLU on 12.01.2020.
 */
public interface BindExternalListToInternalDtoService {

    ExternalDebtBalanceDto bindToExternalDebtBalanceDto(String[] params);

    ExternalClientFeeTransactionDto bindToExternalClientFeeTransactionDto(String[] params);

    ExternalExpenseOpposingPartyLawyerFeeBalanceDto bindToExternalExpenseOpposingPartyLawyerFeeBalanceDto(String[] params);

    ExternalModificationLiableTransactionDto bindToExternalModificationLiableTransactionDto(String[] params);

    PaymentCurrentAccountTransactionDto bindToPaymentCurrentAccountTransactionDto(String[] params);

    ReconciliationFileDto bindToReconciliationFileDto(String[] params);
}
